# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.pyson import Eval, If


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Vps manager Configuration'
    __name__ = 'vps.configuration'
    limit_days_expired = fields.Integer('Limit Days for Expired', required=True)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ])
