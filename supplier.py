# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import datetime, timedelta, date

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateTransition
import psycopg2
from psycopg2 import Error
import os

STATES = {
    'readonly': Eval('state') != 'draft',
}


class SupplierVps(ModelSQL, ModelView):
    'Supplier Vps'
    _rec_name = 'party.name'
    __name__ = 'vps.supplier'
    party = fields.Many2One('party.party', 'Party')
    servers = fields.Function(fields.One2Many('vps.server', None,
            'Servers', add_remove=[]), 'get_servers')

    def get_servers(self, name):
        Server = Pool().get('vps.server')

        servers = Server.search([
            ('supplier.party', '=', self.party),
        ])

        return [server.id for server in servers]

    def get_rec_name(self, name):
        return self.party.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('party.name',) + tuple(clause[1:]),
                ('party.code',) + tuple(clause[1:]),
                ]
